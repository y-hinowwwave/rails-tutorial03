#ベースイメージ
FROM ruby:2.6.0

#使用者
LABEL maintainer y-hino

#インストールなど実行
RUN apt-get update -qq && apt-get install -y  build-essential libpq-dev nodejs
RUN apt-get update -qq && apt-get install -y  postgresql-client

#作業ディレクトリの作成と指定(移動)
RUN  mkdir /rails_tutorial03
WORKDIR /rails_tutorial03

ADD . /rails_tutorial03
